package com.example.daca.redditapp.util;

import android.content.Context;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.squareup.picasso.Picasso;

import java.io.IOException;


public class PicassoLoader {

    Context context;

    public PicassoLoader(Context context) {
        this.context = context;
    }

    public void loadImage(String url, ImageView imageView){
        Picasso.with(context)
                .load(url)
                //.resize(200, 300)
                //.fit()

                //.centerCrop()
                //.rotate(180)
                //.placeholder(R.drawable.placeholder)
                //.error(R.drawable.error)
                .into(imageView);
    }
    public void loadWithGlide(String url, ImageView imageView){
        Glide.with(context)
                .load("https://giant.gfycat.com/NextRedBongo.webm")
                //.asBitmap()
                .asGif()
                .diskCacheStrategy(DiskCacheStrategy.SOURCE)
                .into(imageView);
    }
}
