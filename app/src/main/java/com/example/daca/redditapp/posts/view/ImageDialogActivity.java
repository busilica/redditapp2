package com.example.daca.redditapp.posts.view;

import android.net.Uri;
import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.afollestad.easyvideoplayer.EasyVideoCallback;
import com.afollestad.easyvideoplayer.EasyVideoPlayer;
import com.example.daca.redditapp.R;
import com.example.daca.redditapp.util.PicassoLoader;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

public class ImageDialogActivity extends AppCompatActivity implements View.OnClickListener {

    ImageView image;
    ProgressBar progressBar;
    //WebView webView;
    private EasyVideoPlayer player;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_image_dialog);
        image = (ImageView) findViewById(R.id.imageView_dialog_activity);
        image.setOnClickListener(this);
        progressBar = (ProgressBar) findViewById(R.id.progressBar_dialog_activity);
        String url = getIntent().getStringExtra("url");
        /*webView = (WebView) findViewById(R.id.webView_dialog_activity);

        webView.loadUrl(url);
        webView.setInitialScale(1);
        webView.setWebViewClient(new WebViewClient());
        webView.setWebChromeClient(new WebChromeClient());
        webView.getSettings().setPluginState(WebSettings.PluginState.ON);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setBuiltInZoomControls(true);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setDisplayZoomControls(true);*/
        Picasso.with(this).load(url)

                //.fit()
                //.centerCrop()
                .into(image, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });
        //PicassoLoader loader = new PicassoLoader(this);
        //loader.loadWithGlide(url, image);
    }

    @Override
    public void onClick(View v) {
        finish();
    }

}
