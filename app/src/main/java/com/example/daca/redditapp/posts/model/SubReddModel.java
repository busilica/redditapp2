package com.example.daca.redditapp.posts.model;

import android.content.Context;

import com.example.daca.redditapp.posts.Contract;
import com.example.daca.redditapp.util.VolleyLoader;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Daca on 31.03.2017..
 */

public class SubReddModel implements Contract.SubReddModel {

    private VolleyLoader volleyLoader;
    private ArrayList<String> childrenArray;

    private Contract.Presenter presenter;

    public SubReddModel(Context c, Contract.Presenter presenter) {
        childrenArray = new ArrayList<>();
        volleyLoader = new VolleyLoader(c);;
        this.presenter = presenter;
    }

    @Override
    public void grabSubreddits(String url) {
        Observer<ArrayList<String>> volleyObserver = new Observer<ArrayList<String>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<String> value) {
                childrenArray.addAll(value);
                presenter.populateNavMenu(childrenArray);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        volleyLoader.load(url, volleyObserver, "loadDefaultSubreddits");
    }
}
