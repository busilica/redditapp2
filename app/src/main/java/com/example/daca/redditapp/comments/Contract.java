package com.example.daca.redditapp.comments;

import com.example.daca.redditapp.comments.entities.Comment;
import com.example.daca.redditapp.comments.entities.SearchedSubreddit;

import java.util.ArrayList;

/**
 * Created by Daca on 31.03.2017..
 */

public interface Contract {

    interface Model {
        void loadComments(String url);
        ArrayList<Comment> getComments();
        void searchPosts(String url);
        ArrayList<SearchedSubreddit> getSearchedSubreddits();
    }

    interface View {
        void refreshAdapter();
        void refreshAdapter2();

    }

    interface Presenter {
        void loadComments(String url);
        ArrayList<Comment> getComments();
        void refreshAdapter();
        void setModel(Contract.Model model);
        void searchPosts(String url);
        void refreshAdapter2();
        ArrayList<SearchedSubreddit> getSearchedSubreddits();
    }
}
