package com.example.daca.redditapp.comments.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.daca.redditapp.R;

/**
 * Created by Daca on 05.04.2017..
 */

public class RvHodler extends RecyclerView.ViewHolder {

    ImageView thumbnail;
    TextView title, author, subreddit, domain, upsTime;

    public RvHodler(View itemView) {
        super(itemView);
        thumbnail = (ImageView) itemView.findViewById(R.id.image_thumbnail_custom_row_comments);
        title     = (TextView) itemView.findViewById(R.id.txt_title_custom_row_comments);
        author    = (TextView) itemView.findViewById(R.id.txt_author_custom_row_comments);
        subreddit = (TextView) itemView.findViewById(R.id.txt_subreddit_custom_row_comments);
        domain    = (TextView) itemView.findViewById(R.id.txt_domain_custom_row_comments);
        upsTime   = (TextView) itemView.findViewById(R.id.txt_ups_time_custom_row_comments);
    }
}
