package com.example.daca.redditapp.util;

/**
 * Created by Daca on 17.04.2017..
 */

public class UrlCollections {

    public static final String REDDIT_URL = "https://www.reddit.com";

    public static final String AUTH_URL =
                    "https://www.reddit.com/api/v1/authorize.compact?client_id=%s" +
                    "&response_type=code&state=%s&redirect_uri=%s&" +
                    "duration=permanent&scope=identity,mysubreddits,history";
    public static final String CLIENT_ID = "E-LYekDBfcaoHw";
    public static final String REDIRECT_URI = "http://www.example.com/my_redirect";
    public static final String STATE = "MY_RANDOM_STRING";
    public static final String ACCESS_TOKEN_URL = "https://www.reddit.com/api/v1/access_token";

}
