package com.example.daca.redditapp.posts.model;

import com.example.daca.redditapp.posts.Contract;
import com.example.daca.redditapp.posts.model.entities.Post;


import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Daca on 10.04.2017..
 */

public class SavedPostsModel implements Contract.SavedPostsModel {

    private Contract.SavedPostsPresenter presenter;

    public SavedPostsModel(Contract.SavedPostsPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public Observer<String> getNameObserver() {
        return new Observer<String>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(String name) {
                presenter.getSavedPosts(name);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
    }

    @Override
    public Observer<ArrayList<Post>> getPostsListObserver() {
        return new Observer<ArrayList<Post>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<Post> savedPostsList) {
                presenter.setAdapter(savedPostsList);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
    }

}
