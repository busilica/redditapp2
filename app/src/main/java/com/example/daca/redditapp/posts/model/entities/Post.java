package com.example.daca.redditapp.posts.model.entities;

/**
 * Created by Daca on 24.03.2017..
 */

public class Post {

    private String title;
    private String thumbnail;
    private String permaLink;
    private String url;
    private String preview;
    private String author;
    private String subreddit;
    private String domain;
    private String ups;
    private String comments;
    private String gyf;

    public Post(){

    }

    public Post(String title, String thumbnail, String permaLink, String url, String preview,
                String author, String subreddit, String domain, String ups, String comments, String gyf) {
        this.title = title;
        this.thumbnail = thumbnail;
        this.permaLink = permaLink;
        this.url = url;
        this.preview = preview;
        this.author = author;
        this.subreddit = subreddit;
        this.domain = domain;
        this.ups = ups;
        this.comments = comments;
        this.gyf = gyf;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getPermaLink() {
        return permaLink;
    }

    public void setPermaLink(String permaLink) {
        this.permaLink = permaLink;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getPreview() {
        return preview;
    }

    public void setPreview(String preview) {
        this.preview = preview;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getSubreddit() {
        return subreddit;
    }

    public void setSubreddit(String subreddit) {
        this.subreddit = subreddit;
    }

    public String getDomain() {
        return domain;
    }

    public void setDomain(String domain) {
        this.domain = domain;
    }

    public String getUps() {
        return ups;
    }

    public void setUps(String ups) {
        this.ups = ups;
    }

    public String getComments() {
        return comments;
    }

    public void setComments(String comments) {
        this.comments = comments;
    }

    public String getGyf() {
        return gyf;
    }

    public void setGyf(String gyf) {
        this.gyf = gyf;
    }
}
