package com.example.daca.redditapp.posts;

import android.content.Context;

import com.example.daca.redditapp.posts.model.entities.Post;


import java.util.ArrayList;

import io.reactivex.Observer;

/**
 * Created by Daca on 27.03.2017..
 */

public interface Contract {

    interface Presenter {
        ArrayList<Post> getArrayList();
        void resetArrayList();
        void loadPosts(String url);
        void loadMorePosts(int currentPage, String subreddit);
        void refreshAdapter();
        void setModel(PostsModel postsModel, SubReddModel subReddModel);
        void grabSubreddits();
        void populateNavMenu(ArrayList<String> subReddList);
        void searchPosts(String url);
        void getAccessToken(String code);
    }

    interface SavedPostsPresenter {
        void getSavedPosts(String name);
        void setAdapter(ArrayList<Post> savedPostsList);
        void setModel(Contract.SavedPostsModel model);
        void loadSavedPosts(String token1, String s);
        void loadMorePosts(int currentPage, String SUBREDDIT);
        ArrayList<Post> getArrayList();
    }

    interface View {
        void setAdapter(ArrayList<Post> savedPosts);
        void refreshAdapter();
        void populateNavMenu(ArrayList<String> subReddList);
        Context getContext();
    }

    interface PostsModel {
        void loadPosts(String url);
        void resetPosts();
        ArrayList<Post> getPosts();
        void searchPosts(String url);
        void getAccessToken();
    }

    interface SavedPostsModel {
        Observer<String> getNameObserver();
        Observer<ArrayList<Post>> getPostsListObserver();
    }

    interface SubReddModel{
        void grabSubreddits(String url);
    }

}
