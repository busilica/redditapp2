package com.example.daca.redditapp.posts.view;

import android.app.DialogFragment;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.example.daca.redditapp.R;
import com.example.daca.redditapp.util.PicassoLoader;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.IOException;

/**
 * Created by Daca on 26.03.2017..
 */

public class ImageDialogFragment extends DialogFragment implements View.OnClickListener {


    public ImageDialogFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.image_dialog_fragment, container, false);
        ImageView image = (ImageView) view.findViewById(R.id.imageView_image_dialog_fragment);
        image.setOnClickListener(this);
        final ProgressBar progressBar = (ProgressBar) view.findViewById(R.id.progressBar_image_dialog_fragment);
        //PicassoLoader picasso = new PicassoLoader(getActivity());
        final String url = getArguments().getString("url");
        //int height = 0;

                Picasso.with(getActivity()).load(url)
                .fit()
                //.centerCrop()
                .into(image, new Callback() {
                    @Override
                    public void onSuccess() {
                        progressBar.setVisibility(View.GONE);
                    }

                    @Override
                    public void onError() {

                    }
                });
        return view;
    }

    @Override
    public void onClick(View v) {
        dismiss();
    }
}
