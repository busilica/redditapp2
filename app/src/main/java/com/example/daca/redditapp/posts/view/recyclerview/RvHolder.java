package com.example.daca.redditapp.posts.view.recyclerview;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.example.daca.redditapp.R;

/**
 * Created by Daca on 20.03.2017..
 */

public class RvHolder extends RecyclerView.ViewHolder {

    ImageButton image;
    TextView title;
    TextView author;
    TextView subreddit;
    TextView domain;
    TextView ups;

    public RvHolder(View itemView) {
        super(itemView);
        image = (ImageButton) itemView.findViewById(R.id.imageView);
        title = (TextView) itemView.findViewById(R.id.txt_title_main);
        author = (TextView) itemView.findViewById(R.id.txt_author_main);
        subreddit = (TextView) itemView.findViewById(R.id.txt_subreddit_main);
        domain = (TextView) itemView.findViewById(R.id.txt_domain_main);
        ups = (TextView) itemView.findViewById(R.id.txt_ups_main);
    }
}
