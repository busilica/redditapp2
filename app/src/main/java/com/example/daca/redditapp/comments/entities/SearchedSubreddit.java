package com.example.daca.redditapp.comments.entities;

/**
 * Created by Daca on 12.04.2017..
 */

public class SearchedSubreddit {

    private String title;
    private String description;

    public SearchedSubreddit() {

    }

    public SearchedSubreddit(String title, String description) {
        this.title = title;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
