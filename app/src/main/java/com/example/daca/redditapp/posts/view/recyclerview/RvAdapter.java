package com.example.daca.redditapp.posts.view.recyclerview;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.support.customtabs.CustomTabsIntent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.daca.redditapp.comments.CommentsActivity;
import com.example.daca.redditapp.posts.view.GifActivity;
import com.example.daca.redditapp.util.PicassoLoader;
import com.example.daca.redditapp.R;

import com.example.daca.redditapp.posts.model.entities.Post;

import java.util.ArrayList;

/**
 * Created by Daca on 25.03.2017..
 */

public class RvAdapter extends RecyclerView.Adapter<RvHolder> {

    private ArrayList<Post> postsArrayList;
    private PicassoLoader picasso;
    private Activity activity;

    public RvAdapter(Activity activity, ArrayList<Post> postsArrayList) {
        this.activity = activity;
        this.postsArrayList = postsArrayList;
        this.picasso = new PicassoLoader(activity);
    }

    @Override
    public RvHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.custom_row_posts_main, parent, false);
        return new RvHolder(view);
    }

    @Override
    public void onBindViewHolder(RvHolder holder, int position) {
        final Post post = postsArrayList.get(position);
        String gyf = post.getGyf();
        String imageUrl = post.getPreview();
        if (!imageUrl.equals("self") && !imageUrl.equals("")){
            picasso.loadImage(imageUrl, holder.image);
            //picasso.loadWithGlide(imageUrl, holder.image);
            holder.image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    handleImageClick(post.getUrl(), post.getGyf());
                }
            });
        }else {
            holder.image.setVisibility(View.GONE);
        }
        holder.title.setText(post.getTitle());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handleItemViewClick(post.getPermaLink(), post.getThumbnail(), post.getTitle(),
                                    post.getAuthor(), post.getSubreddit(), post.getDomain());
            }
        });
        holder.author.setText(post.getAuthor() + " -");
        holder.subreddit.setText(post.getSubreddit());
        //holder.domain.setText(" - " + post.getDomain());
        holder.domain.setVisibility(View.GONE);
        String bottomText = post.getUps() + " points - " + post.getComments() + " comments ";
        holder.ups.setText(bottomText);

    }

    @Override
    public int getItemCount() {
        return postsArrayList.size();
    }

    private void handleImageClick(String url, String gif) {

        if (url.contains(".jpg") || url.contains("imgur") || url.contains("gfycat")){
            /*Bundle bundle = new Bundle();
            bundle.putString("url", url);
            android.app.FragmentManager manager = activity.getFragmentManager();
            ImageDialogFragment fragment = new ImageDialogFragment();
            fragment.setArguments(bundle);
            fragment.show(manager, "Dialog");*/

            //intent.putExtra("url", url);
            //activity.startActivity(intent);
            if (gif != null){
                Intent intent = new Intent(activity, GifActivity.class);
                String fig = gif.replaceAll("amp;", "");
                intent.putExtra("url", fig);
                activity.startActivity(intent);
            }else{
                /*Intent intent = new Intent(activity, ImageDialogActivity.class);
                intent.putExtra("url", url);
                activity.startActivity(intent);*/
            }

        }else{
            CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.launchUrl(activity, Uri.parse(url));
        }
    }

    private void handleItemViewClick(String permaLink, String thumbnail, String title, String author,
                                     String subreddit, String domain) {
        Intent intent = new Intent(activity, CommentsActivity.class);
        intent.putExtra("permalink", permaLink);
        intent.putExtra("thumbnail", thumbnail);
        intent.putExtra("title", title);
        intent.putExtra("author", author);
        intent.putExtra("subreddit", subreddit);
        intent.putExtra("domain", domain);
        activity.startActivity(intent);
    }
}
