package com.example.daca.redditapp.util;

import android.content.Context;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.daca.redditapp.comments.entities.Comment;
import com.example.daca.redditapp.comments.entities.SearchedSubreddit;
import com.example.daca.redditapp.posts.model.entities.Post;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;


/**
 * Created by Daca on 27.03.2017..
 */

public class VolleyLoader {

    private Context context;

    public VolleyLoader(Context context) {
        this.context = context;
    }

    public void load(final String url, final Observer observer, final String cmd){
        RequestQueue queue = ConnectionManager.getInstance(context);
        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                switch (cmd) {
                    case "load":
                        Observable<ArrayList<Post>> observable1 = Observable.just(response)
                        .map(new Function<String, ArrayList<Post>>() {
                            @Override
                            public ArrayList<Post> apply(String s) throws Exception {
                                return new JsonParser().parsePosts(s);}})
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());
                        observable1.subscribe(observer);
                        break;
                    case "loadDefaultSubreddits":
                        Observable<ArrayList<String>> observable2 = Observable.just(response)
                        .map(new Function<String, ArrayList<String>>() {
                            @Override
                            public ArrayList<String> apply(String s) throws Exception {
                                return new JsoupParser().parseData(s);}})
                        .subscribeOn(Schedulers.newThread())
                        .observeOn(AndroidSchedulers.mainThread());
                        observable2.subscribe(observer);
                        break;
                    case "loadComments":
                        Observable<ArrayList<Comment>> observable3 = Observable.just(response)
                                        .map(new Function<String, ArrayList<Comment>>() {
                                            @Override
                                            public ArrayList<Comment> apply(String s) throws Exception {
                                                return new JsonParser().parseComments(s);}
                                        })
                                        .subscribeOn(Schedulers.newThread())
                                        .observeOn(AndroidSchedulers.mainThread());
                        observable3.subscribe(observer);
                        break;
                    case "searchSubreddits":
                        Observable<ArrayList<SearchedSubreddit>> observable4 = Observable.just(response)
                                .map(new Function<String, ArrayList<SearchedSubreddit>>() {
                                    @Override
                                    public ArrayList<SearchedSubreddit> apply(String s) throws Exception {
                                        return new JsonParser().parseSearchedSubreddits(s);
                                    }
                                })
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread());
                        observable4.subscribe(observer);
                        break;
                    case "searchPosts":
                        Observable<ArrayList<Post>> observable5 = Observable.just(response)
                                .map(new Function<String, ArrayList<Post>>() {
                                    @Override
                                    public ArrayList<Post> apply(String s) throws Exception {
                                        return new JsonParser().parseSearchedPosts(s);
                                    }
                                })
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread());
                        observable5.subscribe(observer);
                        break;
                }
        };
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
            }
        };
        StringRequest request = new StringRequest(Request.Method.GET, url, listener, errorListener);
        queue.add(request);
    }

    public void loadWithToken(final Observer observer, final String token, String url,
                              final String cmd){
        RequestQueue queue = ConnectionManager.getInstance(context);
        Response.Listener<String> listener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                switch (cmd){
                    case "loadWithToken":
                        Observable<ArrayList<String>> observable1 = Observable.just(response)
                                .map(new Function<String, ArrayList<String>>() {
                                    @Override
                                    public ArrayList<String> apply(String s) throws Exception {
                                        return new JsonParser().parseUserSubreddits(s);}
                                })
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread());
                        observable1.subscribe(observer);
                        break;
                    case "loadUserName":
                        Observable<String> observable2 = Observable.just(response)
                                .map(new Function<String, String>() {
                                    @Override
                                    public String apply(String s) throws Exception {
                                        return new JsonParser().parseUserName(s);}
                                })
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread());
                        observable2.subscribe(observer);
                        break;
                    case "loadSavedPosts":
                        Observable<ArrayList<Post>> observable3 = Observable.just(response)
                                .map(new Function<String, ArrayList<Post>>() {
                                    @Override
                                    public ArrayList<Post> apply(String s) throws Exception {
                                        return new JsonParser().parsePosts(s);}
                                })
                                .subscribeOn(Schedulers.newThread())
                                .observeOn(AndroidSchedulers.mainThread());
                        observable3.subscribe(observer);
                        break;
                }
            }
        };
        Response.ErrorListener errorListener = new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                String e = "";
            }
        };
        StringRequest request = new StringRequest(Request.Method.GET, url, listener, errorListener){
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<String, String>();
                headers.put("Authorization", "bearer " + token);
                return headers;
            }
        };
        queue.add(request);
    }
}
