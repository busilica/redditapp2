package com.example.daca.redditapp.util;

import android.view.Menu;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Daca on 31.03.2017..
 */

public class JsoupParser {

    public ArrayList<String> parseData(String data){
        ArrayList<String> subRedditsList = new ArrayList<>();
        Document doc = Jsoup.parse(data);
        Elements trElements = doc.getElementsByTag("tbody");
        int counter = 1;
        for (int i = 1; i < 31; i++){
            String child = trElements.get(0).childNode(counter).childNode(3).childNode(0)
                    .childNode(0).toString();
            if (!child.equals("")){
                subRedditsList.add(child);
            }
            counter += 2;
        }
        return subRedditsList;
    }
}
