package com.example.daca.redditapp.posts.presenter;

import android.util.Base64;

import com.example.daca.redditapp.posts.Contract;
import com.example.daca.redditapp.posts.model.entities.Post;
import com.example.daca.redditapp.util.JsonParser;
import com.example.daca.redditapp.util.UrlCollections;

import java.util.ArrayList;

/**
 * Created by Daca on 27.03.2017..
 */

public class Presenter implements Contract.Presenter {

    private Contract.View view;
    private Contract.PostsModel postsModel;
    private Contract.SubReddModel subReddModel;

    public Presenter(Contract.View view){
        this.view = view;
    }

    @Override
    public ArrayList<Post> getArrayList() {
        return postsModel.getPosts();
    }

    @Override
    public void resetArrayList() {
        postsModel.resetPosts();
        view.refreshAdapter();
    }

    @Override
    public void loadPosts(String url) {
        postsModel.loadPosts(url);
    }

    @Override
    public void loadMorePosts(int currentPage, String subreddit) {
        JsonParser parser = new JsonParser();
        String count = "?count=" + String.valueOf((currentPage - 1) * 25);
        String after = "&after=" + parser.getAFTER();
        String url = UrlCollections.REDDIT_URL + subreddit + ".json" + count + after;
        postsModel.loadPosts(url);
    }

    @Override
    public void refreshAdapter() {
        view.refreshAdapter();
    }

    public void setModel(Contract.PostsModel postsModel, Contract.SubReddModel subReddModel) {
        this.postsModel = postsModel;
        this.subReddModel = subReddModel;
    }

    @Override
    public void grabSubreddits() {
        String url = "http://redditmetrics.com/top";
        subReddModel.grabSubreddits(url);
    }

    @Override
    public void populateNavMenu(ArrayList<String> subReddList) {
        view.populateNavMenu(subReddList);
    }

    @Override
    public void searchPosts(String url) {
        postsModel.searchPosts(url);
    }

    @Override
    public void getAccessToken(String code) {
        //we need Base64 encoded authentication string which contains our CLIENT_ID and a blank
        //password, separeted by a :
        String authString = UrlCollections.CLIENT_ID + ":";
        final String encodedAuthString = Base64.encodeToString(authString.getBytes(), Base64.NO_WRAP);
        //we can now build a Request object to connect to the ACCESS_TOKEN_URL
        //you must pass the encodedAuthString as a header of the request.
        //it's also a good idea to include a custom User-Agent header
        //you must pass the code as POST data

    }
}
