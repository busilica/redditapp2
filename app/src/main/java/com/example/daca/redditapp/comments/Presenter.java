package com.example.daca.redditapp.comments;

import com.example.daca.redditapp.comments.entities.Comment;
import com.example.daca.redditapp.comments.entities.SearchedSubreddit;
import com.example.daca.redditapp.util.VolleyLoader;

import java.util.ArrayList;

/**
 * Created by Daca on 31.03.2017..
 */

public class Presenter implements Contract.Presenter {

    private Contract.Model model;
    private Contract.View view;

    public Presenter(Contract.View view) {
        this.view = view;
    }

    @Override
    public void loadComments(String url) {
        model.loadComments(url);
    }

    @Override
    public ArrayList<Comment> getComments() {
        return model.getComments();
    }

    @Override
    public void refreshAdapter() {
        view.refreshAdapter();
    }

    @Override
    public void setModel(Contract.Model model) {
        this.model = model;
    }

    @Override
    public void searchPosts(String url) {
        model.searchPosts(url);
    }

    @Override
    public void refreshAdapter2() {
        view.refreshAdapter2();
    }

    @Override
    public ArrayList<SearchedSubreddit> getSearchedSubreddits() {
        return model.getSearchedSubreddits();
    }
}
