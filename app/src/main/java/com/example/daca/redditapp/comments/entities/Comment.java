package com.example.daca.redditapp.comments.entities;

/**
 * Created by Daca on 13.04.2017..
 */

public class Comment {

    private String author;
    private String comment;
    private String ups;
    private String postedTime;

    public Comment() {

    }

    public Comment(String author, String comment, String ups, String postedTime) {
        this.author = author;
        this.comment = comment;
        this.ups = ups;
        this.postedTime = postedTime;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getUps() {
        return ups;
    }

    public void setUps(String ups) {
        this.ups = ups;
    }

    public String getPostedTime() {
        return postedTime;
    }

    public void setPostedTime(String postedTime) {
        this.postedTime = postedTime;
    }
}
