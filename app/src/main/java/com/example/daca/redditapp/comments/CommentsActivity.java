package com.example.daca.redditapp.comments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.daca.redditapp.comments.recyclerview.RvAdapter;
import com.example.daca.redditapp.R;
import com.example.daca.redditapp.util.PicassoLoader;

public class CommentsActivity extends AppCompatActivity implements Contract.View {

    private Contract.Presenter presenter;
    private Contract.Model model;

    private RvAdapter adapter;
    private RecyclerView recyclerView;

    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);
        setupMVP();
        recyclerView = (RecyclerView) findViewById(R.id.rv_comments_activity);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        //adapter = new RvAdapter(presenter.getComments());
        //recyclerView.setAdapter(adapter);
        intent = getIntent();
        if (intent.hasExtra("url")){
            presenter.searchPosts(intent.getStringExtra("url"));
        }else {
            String url = "https://www.reddit.com" + intent.getStringExtra("permalink") + ".json";
            presenter.loadComments(url);
        }
    }

    @Override
    protected void onDestroy() {
        presenter = null;
        model = null;
        super.onDestroy();
    }

    private void setupMVP() {
        presenter = new Presenter(this);
        model = new Model(this, presenter);
        presenter.setModel(model);
    }

    @Override
    public void refreshAdapter() {
        /*if (adapter == null){
            adapter = new RvAdapter(commentsList);
            recyclerView.setAdapter(adapter);
        }
        adapter.notifyDataSetChanged();*/
        adapter = new RvAdapter(presenter.getComments(), intent, this);
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void refreshAdapter2() {
        adapter = new RvAdapter(presenter.getSearchedSubreddits(), intent, this);
        recyclerView.setAdapter(adapter);
    }
}
