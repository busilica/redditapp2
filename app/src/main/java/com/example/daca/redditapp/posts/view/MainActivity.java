package com.example.daca.redditapp.posts.view;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.customtabs.CustomTabsIntent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.example.daca.redditapp.comments.CommentsActivity;
import com.example.daca.redditapp.posts.Contract;
import com.example.daca.redditapp.posts.model.PostsModel;
import com.example.daca.redditapp.posts.model.SavedPostsModel;
import com.example.daca.redditapp.posts.model.SubReddModel;
import com.example.daca.redditapp.posts.model.entities.Post;
import com.example.daca.redditapp.posts.presenter.Presenter;
import com.example.daca.redditapp.R;
import com.example.daca.redditapp.posts.presenter.SavedPostsPresenter;
import com.example.daca.redditapp.posts.view.recyclerview.EndlessRecyclerOnScrollListener;
import com.example.daca.redditapp.posts.view.recyclerview.RvAdapter;


import com.example.daca.redditapp.util.ConnectionManager;
import com.example.daca.redditapp.util.UrlCollections;
import com.example.daca.redditapp.util.VolleyLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;


public class MainActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, Contract.View {


    private NavigationView navigationView;

    private Menu navMenu;

    private RecyclerView recyclerView;
    private RvAdapter adapter;
    private EndlessRecyclerOnScrollListener scrollListener;

    private Contract.Presenter presenter;
    private Contract.PostsModel postsModel;
    private Contract.SubReddModel subReddModel;
    private Contract.SavedPostsPresenter savedPostsPresenter;
    private Contract.SavedPostsModel savedPostsModel;

    private String SUBREDDIT = "";
    private String token1 = "";
    private boolean isLoggedIn = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setupMVP();
        recyclerView = (RecyclerView) findViewById(R.id.recyclerView);
        setupRecyclerView();
        SUBREDDIT = "/r/popular";
        String url = UrlCollections.REDDIT_URL + SUBREDDIT + ".json";
        presenter.loadPosts(url);
        presenter.grabSubreddits();
        setupNavDrawer();
        navMenu = navigationView.getMenu();
        if (!isLoggedIn){
            navMenu.add("Log In");
        }
        navMenu.add("/r/popular");
    }

    @Override
    protected void onResume() {
        super.onResume();
        //navMenu.clear();
        if (getIntent()!=null && getIntent().getAction().equals(Intent.ACTION_VIEW)){
            Uri uri = getIntent().getData();
            if (uri.getQueryParameter("error")!=null){
                String error = uri.getQueryParameter("error");
                Log.e("Error: ", "An eror has occured :" + error);
            }else {
                String state = uri.getQueryParameter("state");
                if (state.equals(UrlCollections.STATE)){
                    String code = uri.getQueryParameter("code");
                    getAccessToken(code);
                }
            }
        }
    }

    private void getAccessToken(final String code) {
        presenter.getAccessToken(code);
        //we need Base64 encoded authentication string which contains our CLIENT_ID and a blank
        //password, separeted by a :
        String authString = UrlCollections.CLIENT_ID + ":";
        final String encodedAuthString = Base64.encodeToString(authString.getBytes(), Base64.NO_WRAP);
        //we can now build a Request object to connect to the ACCESS_TOKEN_URL
        //you must pass the encodedAuthString as a header of the request.
        //it's also a good idea to include a custom User-Agent header
        //you must pass the code as POST data
        final Observer<ArrayList<String>> observer = new Observer<ArrayList<String>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<String> value) {
                navMenu.clear();
                navMenu.add("saved");
                navMenu.add("upvoted");
                navMenu.add("/r/popular");
                isLoggedIn = true;
                populateNavMenu(value);
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        RequestQueue queue = ConnectionManager.getInstance(this);
        queue.add(new StringRequest(Request.Method.POST, UrlCollections.ACCESS_TOKEN_URL
                , new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                String s = response;
                try {
                    JSONObject object1 = new JSONObject(response);
                    String token = object1.getString("access_token");
                    token1 = token;
                    final String URL = "https://oauth.reddit.com/subreddits/mine/subscriber.json?limit=100";
                    new VolleyLoader(MainActivity.this).loadWithToken(observer, token, URL,
                                                                           "loadWithToken");
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        }){
            @Override
            public String getBodyContentType() {
                return /*"application/x-www-form-urlencoded";*/ "application/x-www-form-urlencoded; charset=utf-8";
            }

            @Override
            public byte[] getBody() throws AuthFailureError {
                try {
                    return ("grant_type=authorization_code&code=" + code +
                            "&redirect_uri=" + UrlCollections.REDIRECT_URI).getBytes("utf-8");
                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                    return null;
                }
            }
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String, String> headers = new HashMap<>();
                headers.put("User-Agent", "Sample App");
                headers.put("Authorization", "Basic " + encodedAuthString);
                return headers;
            }
        });
    }

    private void setupNavDrawer() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    private void setupRecyclerView() {
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);
        scrollListener = new EndlessRecyclerOnScrollListener(linearLayoutManager) {
            @Override
            public void onLoadMore(int currentPage) {
                if (SUBREDDIT.equals("saved") || SUBREDDIT.equals("upvoted")){
                    savedPostsPresenter.loadMorePosts(currentPage, SUBREDDIT);
                }else{
                    presenter.loadMorePosts(currentPage, SUBREDDIT);
                }
            }
        };
        recyclerView.addOnScrollListener(scrollListener);
        adapter = new RvAdapter(MainActivity.this, presenter.getArrayList());
        recyclerView.setAdapter(adapter);
    }

    private void setupMVP() {
        presenter = new Presenter(this);
        postsModel = new PostsModel(this, presenter);
        subReddModel = new SubReddModel(this, presenter);
        presenter.setModel(postsModel, subReddModel);
        savedPostsPresenter = new SavedPostsPresenter(this);
        savedPostsModel = new SavedPostsModel(savedPostsPresenter);
        savedPostsPresenter.setModel(savedPostsModel);
    }

    @Override
    protected void onDestroy() {
        presenter = null;
        postsModel = null;
        subReddModel = null;
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    /*@Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        /*if (id == R.id.action_settings) {
            return true;
        }
        if (id == R.id.action_search_subreddits){
            AlertDialog.Builder search = new AlertDialog.Builder(this);
            search.setTitle("Search subreddits");
            final EditText edtSearch = new EditText(this);
            search.setView(edtSearch);
            search.setPositiveButton("SEARCH", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String edt = edtSearch.getText().toString().toLowerCase().trim();
                    String url = "https://www.reddit.com/subreddits/search.json?q="+edt;
                    startActivity(new Intent(MainActivity.this, CommentsActivity.class)
                                            .putExtra("url", url));
                }
            });
            search.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            search.show();
        }
        /*if (id == R.id.action_search_posts){
            AlertDialog.Builder search = new AlertDialog.Builder(this);
            search.setTitle("Search posts");
            final EditText edtSearch = new EditText(this);
            search.setView(edtSearch);
            search.setPositiveButton("SEARCH", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String edt = edtSearch.getText().toString().toLowerCase().trim();
                    String url = "https://www.reddit.com/subreddits/search.json?q="+edt;
                    presenter.searchPosts(url);

                }
            });
            search.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            search.show();
        }
        return super.onOptionsItemSelected(item);
    }*/

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        String title = item.getTitle().toString();
        if (title.equals("Log In")){
            String url = String.format(UrlCollections.AUTH_URL, UrlCollections.CLIENT_ID
                    , UrlCollections.STATE, UrlCollections.REDIRECT_URI);
            //startActivity(new Intent(this, LogInActivity.class).putExtra("url", url));
            /*CustomTabsIntent.Builder builder = new CustomTabsIntent.Builder();
            CustomTabsIntent customTabsIntent = builder.build();
            customTabsIntent.launchUrl(this, Uri.parse(url));*/
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));

        }else if (title.equals("saved")){
            SUBREDDIT = "saved";
            savedPostsPresenter.loadSavedPosts(token1, "saved");
        }else if (title.equals("upvoted")){
            SUBREDDIT = "upvoted";
            savedPostsPresenter.loadSavedPosts(token1, "upvoted");
        }else {
            adapter = null;
            SUBREDDIT = title;
            String url = UrlCollections.REDDIT_URL + SUBREDDIT + ".json";
            scrollListener.setCurrentPage(1);
            presenter.resetArrayList();
            presenter.loadPosts(url);
        }


        /*int id = item.getItemId();

        if (id == R.id.nav_camera) {
            // Handle the camera action
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_slideshow) {

        } else if (id == R.id.nav_manage) {

        } else if (id == R.id.nav_share) {

        } else if (id == R.id.nav_send) {

        }*/

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void setAdapter(ArrayList<Post> savedPosts) {
        ArrayList<Post> currentList = savedPostsPresenter.getArrayList();
        if (currentList.size() > 25){
            currentList.addAll(savedPosts);
            adapter.notifyDataSetChanged();
        }else{
            adapter = null;
            RvAdapter adapter = new RvAdapter(this, savedPosts);
            recyclerView.setAdapter(adapter);
        }

    }

    @Override
    public void refreshAdapter() {
        if (adapter == null){
            adapter = new RvAdapter(this, presenter.getArrayList());
            recyclerView.setAdapter(adapter);
        }
        adapter.notifyDataSetChanged();
    }

    @Override
    public void populateNavMenu(ArrayList<String> subReddList) {
        for (int i = 0; i < subReddList.size(); i++){
            navMenu.add(subReddList.get(i));
        }
    }

    @Override
    public Context getContext() {
        return this;
    }

}
