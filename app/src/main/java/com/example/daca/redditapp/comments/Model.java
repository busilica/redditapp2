package com.example.daca.redditapp.comments;

import android.content.Context;

import com.example.daca.redditapp.comments.entities.Comment;
import com.example.daca.redditapp.comments.entities.SearchedSubreddit;
import com.example.daca.redditapp.util.VolleyLoader;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Daca on 31.03.2017..
 */

public class Model implements Contract.Model {
    private Context c;
    private Contract.Presenter presenter;
    private ArrayList<Comment> commentsList;
    private ArrayList<SearchedSubreddit> searchedSubreddits;

    public Model(Context c, Contract.Presenter presenter) {
        this.c = c;
        this.presenter = presenter;
        commentsList = new ArrayList<>();
        searchedSubreddits = new ArrayList<>();
    }

    @Override
    public void loadComments(String url) {
        VolleyLoader volleyLoader = new VolleyLoader(c);
        Observer<ArrayList<Comment>> volleyObserver = new Observer<ArrayList<Comment>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<Comment> value) {
                //commentsList.addAll(value);
                for (int i = 0; i < value.size(); i++){
                    commentsList.add(value.get(i));
                }
                presenter.refreshAdapter();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        volleyLoader.load(url, volleyObserver, "loadComments");
    }

    @Override
    public ArrayList<Comment> getComments() {
        return commentsList;
    }


    @Override
    public void searchPosts(String url) {
        Observer<ArrayList<SearchedSubreddit>> observer = new Observer<ArrayList<SearchedSubreddit>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<SearchedSubreddit> value) {
                for (int i = 0; i < value.size(); i++){
                    searchedSubreddits.add(value.get(i));
                }
                presenter.refreshAdapter2();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        new VolleyLoader(c).load(url, observer, "searchSubreddits");
    }

    @Override
    public ArrayList<SearchedSubreddit> getSearchedSubreddits() {
        return searchedSubreddits;
    }
}
