package com.example.daca.redditapp.comments.recyclerview;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.daca.redditapp.R;
import com.example.daca.redditapp.comments.CommentsActivity;
import com.example.daca.redditapp.comments.entities.Comment;
import com.example.daca.redditapp.comments.entities.SearchedSubreddit;
import com.example.daca.redditapp.util.PicassoLoader;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Daca on 05.04.2017..
 */

public class RvAdapter extends RecyclerView.Adapter<RvHodler> {

    private ArrayList<Comment> commentsList;
    private ArrayList<SearchedSubreddit> searchedSubreddits;
    private Intent intent;
    private Activity activity;
    private LayoutInflater inflater;

    public RvAdapter(ArrayList<Comment> commentsList, Intent intent, Activity activity) {
        this.commentsList = commentsList;
        this.intent = intent;
        this.activity = activity;
        inflater = activity.getLayoutInflater();
    }

    public RvAdapter(ArrayList<SearchedSubreddit> searchedSubreddits, Intent intent,
                     CommentsActivity activity) {
        this.searchedSubreddits = searchedSubreddits;
        this.intent = intent;
        this.activity = activity;
        inflater = activity.getLayoutInflater();
    }

    @Override
    public RvHodler onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.custom_row_activity_saved, parent, false);
        return new RvHodler(view);
    }

    @Override
    public void onBindViewHolder(RvHodler holder, int position) {
        if (intent.hasExtra("url")){
            SearchedSubreddit subreddit = searchedSubreddits.get(position);
            holder.thumbnail.setVisibility(View.GONE);
            holder.subreddit.setVisibility(View.GONE);
            holder.domain.setVisibility(View.GONE);
            holder.upsTime.setVisibility(View.GONE);
            holder.title.setText(subreddit.getTitle());
            holder.author.setText(subreddit.getDescription());
        }else {
            if (position == 0){
                holder.upsTime.setVisibility(View.GONE);
                holder.title.setText(intent.getStringExtra("title"));
                holder.author.setText(intent.getStringExtra("author"));
                holder.subreddit.setText(intent.getStringExtra("subreddit"));
                holder.domain.setText(intent.getStringExtra("domain"));
                String thumbnail = intent.getStringExtra("thumbnail");
                if (!thumbnail.equals("")){
                    Picasso.with(activity).load(thumbnail).into(holder.thumbnail);
                }else {
                    holder.thumbnail.setVisibility(View.GONE);
                }
            }else {
                Comment comment = commentsList.get(position);
                holder.thumbnail.setVisibility(View.GONE);
                holder.subreddit.setVisibility(View.GONE);
                holder.domain.setVisibility(View.GONE);
                holder.title.setText(comment.getAuthor());
                holder.author.setText(comment.getComment());
                holder.upsTime.setText(comment.getUps());
            }
        }
    }

    @Override
    public int getItemCount() {
        if (commentsList == null){
            return searchedSubreddits.size();
        }else {
            return commentsList.size();
        }
    }
}
