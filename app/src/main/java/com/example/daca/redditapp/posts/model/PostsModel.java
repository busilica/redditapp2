package com.example.daca.redditapp.posts.model;

import android.content.Context;

import com.example.daca.redditapp.posts.Contract;
import com.example.daca.redditapp.posts.model.entities.Post;
import com.example.daca.redditapp.util.VolleyLoader;

import java.util.ArrayList;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

/**
 * Created by Daca on 31.03.2017..
 */

public class PostsModel implements Contract.PostsModel {

    private ArrayList<Post> postsArrayList;
    private VolleyLoader volleyLoader;
    private Contract.Presenter presenter;

    public PostsModel(Context c, Contract.Presenter presenter) {
        postsArrayList = new ArrayList<>();
        volleyLoader = new VolleyLoader(c);;
        this.presenter = presenter;
    }

    @Override
    public void loadPosts(String url) {
        Observer<ArrayList<Post>>volleyObserver =
                new Observer<ArrayList<Post>>() {
            @Override
            public void onSubscribe(Disposable d) {
            }
            @Override
            public void onNext(ArrayList<Post> value) {
                for (int i = 0; i < value.size(); i++){
                    postsArrayList.add(value.get(i));
                }
                //postsArrayList.addAll(value);
                presenter.refreshAdapter();

            }
            @Override
            public void onError(Throwable e) {
            }
            @Override
            public void onComplete() {
            }
        };
        volleyLoader.load(url, volleyObserver, "load");
    }

    @Override
    public void resetPosts() {
        postsArrayList = null;
        postsArrayList = new ArrayList<>();
    }

    @Override
    public ArrayList<Post> getPosts() {
        return postsArrayList;
    }

    @Override
    public void searchPosts(String url) {
        Observer<ArrayList<Post>> observer = new Observer<ArrayList<Post>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(ArrayList<Post> value) {
                postsArrayList = null;
                postsArrayList = value;
                presenter.refreshAdapter();
            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
        volleyLoader.load(url, observer, "searchPosts");
    }

    @Override
    public void getAccessToken() {

}


}
