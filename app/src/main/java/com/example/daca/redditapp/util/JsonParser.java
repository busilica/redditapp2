package com.example.daca.redditapp.util;


import com.example.daca.redditapp.comments.entities.Comment;
import com.example.daca.redditapp.comments.entities.SearchedSubreddit;
import com.example.daca.redditapp.posts.model.entities.Post;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import io.reactivex.Observer;

/**
 * Created by Daca on 27.03.2017..
 */

public class JsonParser {

    private Observer<Post> jsonObserver;
    private static String AFTER;

    public ArrayList<Post> parsePosts(String data){
        ArrayList<Post> postsList = new ArrayList<>();
        try {
            JSONObject object1 = new JSONObject(data).getJSONObject("data");
            AFTER = object1.getString("after");
            JSONArray array = object1.getJSONArray("children");
            for (int i = 0; i < array.length(); i++){
                JSONObject object2 = array.getJSONObject(i).getJSONObject("data");
                String title = object2.getString("title");
                String thumbnail = object2.getString("thumbnail");
                String permaLink = object2.getString("permalink");
                String url = object2.getString("url");
                String preview = null;
                String gif = "";
                if (object2.has("preview")){
                    JSONObject previewObject = object2.getJSONObject("preview");
                    JSONArray images = previewObject.getJSONArray("images");
                    JSONObject urlObject = images.getJSONObject(0);
                    JSONObject source = urlObject.getJSONObject("source");
                    if (urlObject.has("variants")){
                        JSONObject variantObject = urlObject.getJSONObject("variants");
                        if (variantObject.has("mp4")){
                            //preview = thumbnail;
                            JSONObject gifObject = variantObject.getJSONObject("mp4");
                            JSONObject sourceObject = gifObject.getJSONObject("source");
                            //JSONArray resolutions = gifObject.getJSONArray("resolutions");
                            //JSONObject sourceObject = null;
                            //sourceObject = (JSONObject) resolutions.get(1);
                            preview = thumbnail;
                            gif = sourceObject.getString("url");
                        }else{
                            gif = null;
                            preview = source.getString("url");
                        }
                    }else {
                        gif = null;
                    }
                    
                }else {
                    preview = "";
                }
                String author = object2.getString("author");
                String subreddit = object2.getString("subreddit");
                String domain = object2.getString("domain");
                String ups = object2.getString("ups");
                String comments = object2.getString("num_comments");

                postsList.add(new Post(title, thumbnail, permaLink, url, preview, author, subreddit,
                                       domain, ups, comments, gif));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return postsList;
    }

    public ArrayList<Comment> parseComments(String data){
        ArrayList<Comment> commentsList = new ArrayList<>();
        try {
            JSONArray array = new JSONArray(data);
            int size = array.getJSONObject(1).getJSONObject("data").getJSONArray("children").length();
            JSONObject object = array.getJSONObject(1).getJSONObject("data");
            JSONArray array2 = object.getJSONArray("children");
            for (int i = 0; i < size; i++){
                JSONObject object2 = array2.getJSONObject(i).getJSONObject("data");
                if (object2.has("body")){
                    String author = object2.getString("author");
                    String comment = object2.getString("body");
                    String ups = object2.getString("ups");
                    String postedTime = object2.getString("created_utc");
                    commentsList.add(new Comment(author, comment, ups, postedTime));
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return commentsList;
    }

    public String getAFTER() {
        return AFTER;
    }

    public ArrayList<String> parseUserSubreddits(String data){
        ArrayList<String> subredditsList = new ArrayList<>();
        try {
            JSONObject object1 = new JSONObject(data);
            JSONObject object2 = object1.getJSONObject("data");
            JSONArray array1 = object2.getJSONArray("children");
            for (int i = 0; i < array1.length(); i++){
                JSONObject object3 = (JSONObject) array1.get(i);
                JSONObject object4 = object3.getJSONObject("data");
                String title = object4.getString("url");
                subredditsList.add(title);
            }
            String test = "";
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return subredditsList;
    }

    public String parseUserName(String data){
        String name = "";
        try {
            JSONObject object1 = new JSONObject(data);
            name = object1.getString("name");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return name;
    }

    public ArrayList<SearchedSubreddit> parseSearchedSubreddits(String data){
        ArrayList<SearchedSubreddit> list = new ArrayList<>();
        try {
            JSONObject object = new JSONObject(data);
            JSONObject object1 = object.getJSONObject("data");
            JSONArray array1 = object1.getJSONArray("children");
            for (int i = 0; i < array1.length(); i++){
                JSONObject object2 = array1.getJSONObject(i);
                JSONObject object3 = object2.getJSONObject("data");
                String subreddit = object3.getString("display_name_prefixed");
                String description = object3.getString("public_description");
                list.add(new SearchedSubreddit(subreddit, description));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<Post> parseSearchedPosts(String data){
        ArrayList<Post> postsArrayList = new ArrayList<>();
        try {
            JSONObject object1 = new JSONObject(data);
            JSONObject object2 = object1.getJSONObject("data");
            JSONArray array1 = object2.getJSONArray("children");
            for (int i = 0; i < array1.length(); i++){
                JSONObject object3 = array1.getJSONObject(i);
                JSONObject object4 = object3.getJSONObject("data");
                String title = object4.getString("title");
                String thumbnail = "";/*object4.getString("thumbnail")*/;
                String permaLink = object4.getString("permalink");
                String url = object4.getString("url");
                String author = object4.getString("author");
                String subreddit = object2.getString("subreddit");
                String domain = object2.getString("domain");
                String ups = object2.getString("ups");
                String comments = object2.getString("num_comments");
                String preview = "";
                String gyf = "";
                postsArrayList.add(new Post(title, thumbnail, permaLink, url, preview,
                                            author, subreddit, domain, ups, comments, gyf));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return postsArrayList;
    }
}
