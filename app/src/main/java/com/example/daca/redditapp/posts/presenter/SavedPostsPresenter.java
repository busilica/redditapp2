package com.example.daca.redditapp.posts.presenter;

import android.content.Context;


import com.example.daca.redditapp.posts.Contract;
import com.example.daca.redditapp.posts.model.entities.Post;
import com.example.daca.redditapp.util.JsonParser;
import com.example.daca.redditapp.util.VolleyLoader;

import java.util.ArrayList;

/**
 * Created by Daca on 10.04.2017..
 */

public class SavedPostsPresenter implements Contract.SavedPostsPresenter {
    private Contract.View view;
    private Contract.SavedPostsModel model;
    private String token;
    private Context context;
    private static String savedOrUpvoted = "";
    private static String userName = "";
    private ArrayList<Post> postsList;

    public SavedPostsPresenter(Contract.View view) {
        this.view = view;
        context = view.getContext();
    }

    @Override
    public void getSavedPosts(String name) {
        this.userName = name;
        final String URL = "https://oauth.reddit.com/user/" + name + "/" + savedOrUpvoted + ".json";
        new VolleyLoader(context).loadWithToken(model.getPostsListObserver(), token, URL,
                                                     "loadSavedPosts");
    }

    @Override
    public void setAdapter(ArrayList<Post> savedPostsList) {
        postsList = savedPostsList;
        view.setAdapter(savedPostsList);
    }

    @Override
    public void setModel(Contract.SavedPostsModel model) {
        this.model = model;
    }

    @Override
    public void loadSavedPosts(String token1, String s) {
        this.savedOrUpvoted = s;
        token = token1;
        final String URL = "https://oauth.reddit.com/api/v1/me";
        new VolleyLoader(context).loadWithToken(model.getNameObserver(), token, URL,
                                                     "loadUserName");
                //loadUserName(model.getNameObserver(), token);
    }

    @Override
    public void loadMorePosts(int currentPage, String SUBREDDIT) {
        JsonParser parser = new JsonParser();
        String count = "?count=" + String.valueOf((currentPage - 1) * 25);
        String after = "&after=" + parser.getAFTER();
        String url = "";
        if (SUBREDDIT.equals("saved")){
            url = "https://oauth.reddit.com/user/" + userName + "/saved.json" + count + after;
            new VolleyLoader(context).loadWithToken(model.getPostsListObserver(), url, token
                                                         ,"loadSavedPosts");

        }else if (SUBREDDIT.equals("upvoted")){
            url = "https://oauth.reddit.com/user/" + userName + "/upvoted.json" + count + after;
            new VolleyLoader(context).loadWithToken(model.getPostsListObserver(), url, token
                                                         ,"loadSavedPosts");

        }
    }

    @Override
    public ArrayList<Post> getArrayList() {
        return postsList;
    }
}
